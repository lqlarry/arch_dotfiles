#!/bin/bash
wikidir="/usr/share/doc/arch-wiki/html/en/"
wikidocs="$(find ${wikidir} -iname "*.html")"
main() {
        choice=$(printf '%s\n' "${wikidocs[@]}" | \ 
                cut -d "/" -f8- | \
                /usr/local/bin/dmenu -i -l 20 -p 'Arch Wiki Docs: ' ) || exit 0
        if [ "$choice" ]; then
                qutebrowser "${wikidir}${choice}"
        else
                echo "Program terminated." && exit 0
        fi
}
main
