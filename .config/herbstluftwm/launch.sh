#!/usr/bin/env sh

# Terminate already running bar instances
pkill polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

# Launch
sleep 5 & exec polybar hlwm 2>~/.config/polybar/logfile.txt &

echo "Bar launched..."
