# ~/.bashrc

export PATH="$HOME/.local/bin:$PATH"
export EDITOR="/usr/bin/nvim"

source ~/.aliases

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '
# PS1="\e[31m\]\u - \e[34m\]\@\n \e[33m\]\w \e[00m\]$ "
# PS1="\'if [ \$? = 0 ]; then echo ✅; else echo ❌; fi\' \e[31m\]\u - \e[34m\]\@\n     \e[33m\]\w \e[00m\]$ "

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
